var express = require('express'),
    cors = require('cors'),
    passport = require('passport'),
    routes = require('./routes/routes'),
    bodyParser = require('body-parser'),
    actions = require('./routes/email'),
    user = require('./routes/user');


var app = express();
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', "Origin, X-Requested-With, Content-Type, Accept, x-auth");
    next();
});

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(routes);
app.use(passport.initialize());

app.listen(8080, function () {
    console.log('server is running');
});
routes.post('/sendmail', actions.sendmail);
routes.get('/user/me', user.me);
routes.get('/user/me/skills', user.skills);
routes.get('/user/me/experience', user.experience);
routes.get('/user/me/education', user.education);
//app.route('/sendmail').get(cors(), actions.sendmail);
