var nodeMailer = require('nodemailer');
module.exports = {
    sendmail: function (req, res) {
        console.log('Received request');
        let transporter = nodeMailer.createTransport({
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            auth: {
                user: '',
                pass: ''
            }
        });

        // send the message and get a callback with an error or details of the message that was sent
        var mailOptions = {
            from: req.body.email,
            to: "alejsimpson@gmail.com",
            subject: req.body.subject,
            text: "Email from: " + req.body.name + "(" + req.body.email + ")" + " \r" + req.body.message,
        }
        transporter.sendMail(mailOptions, function (err, msg) {
            console.log("Message:" + JSON.stringify(msg));
            if (err){
                console.log(err);
                return res.json({ success: false, status: 'fail', message: 'Message not sent' });
            }
            else{
                return res.json({ success: true, status: 'sent', message: 'Message sent' });
            }
        });
    }
}