module.exports = {
    me: (req, res) => {
        console.log('Requesting user information');
        var obj = {
            fName: "Alejandro",
            lName: "Fernández",
            title: "Senior DevOps Engineer & Full-Stack Developer",
            birth: "13 October 1991",
            email: "alejandrofdez@me.com",
            phone: "(+34) 600 462 408",
            location: "Barcelona, Spain",
            languages: ["English", "Spanish", "Catalan"],
            bitbucket: "l1qu1d1c3", // Just the username
            linkedin: "alejandro-fernandez-artidiello" // What comes after the /in/ on the LI URL
        }
        return res.json(obj);
    },

    skills: (req, res) => {
        console.log('Loading skills');
        var skills = [
            {
                'id': '1',
                'skill': 'Amazon Web Services',
                'progress': '100%'
            },
            {
                'id': '2',
                'skill': 'Microsoft Azure',
                'progress': '80%'
            },
            {
                'id': '3',
                'skill': 'JavaScript / NodeJS',
                'progress': '90%'
            },
            {
                'id': '4',
                'skill': 'Elastic (ELK)',
                'progress': '75%'
            },
            {
                'id': '5',
                'skill': 'Continuous Integration & Delivery',
                'progress': '100%'
            },
            {
                'id': '6',
                'skill': 'PowerShell & Bash Scripting',
                'progress': '95%'
            },
            {
                'id': '7',
                'skill': 'Team Building',
                'progress': '100%'
            },
            {
                'id': '8',
                'skill': 'Knowledge Management',
                'progress': '100%'
            },
            {
                'id': '9',
                'skill': 'Budget Monitoring',
                'progress': '100%'
            },
            {
                'id': '10',
                'skill': 'Automation',
                'progress': '100%'
            },
            {
                'id': '11',
                'skill': 'Test Driven Development',
                'progress': '90%'
            },
            {
                'id': '12',
                'skill': 'Cloud Computing',
                'progress': '100%'
            },
            {
                'id': '13',
                'skill': 'Team Spirit',
                'progress': '100%'
            },
            {
                'id': '14',
                'skill': 'Kubernetes',
                'progress': '80%'
            },
            {
                'id': '15',
                'skill': 'Docker',
                'progress': '90%'
            },
            {
                'id': '16',
                'skill': 'Microservices',
                'progress': '100%'
            }
        ];
        return res.json(skills);
    },

    experience: (req, res) => {
        console.log('Loading Experience data');
        var experience = [
            {
                'id': '1',
                'from_to_month_year': 'November 2018 - Present',
                'organization': 'OGUN',
                'logo': 'ogun.png',
                'height': '"50px"',
                'designation': 'Lead Senior DevOps Engineer',
                'details': `As a DevOps Engineer, my goal is to make the Developer's job easier, implementing tools that help <strong>automate</strong> any and all manual and repetitive processes. I also provide the company with <strong>cost-saving solutions</strong> and optimal tools for the day to day work. 
                My role also involves the integration of <strong>automated</strong> deployment systems, <strong>centralised</strong> configuration and <strong>optimisation of infrastructure</strong>, using tools such as:
                AWS (EC2, S3, RDS, R53, etc...).
                <br>- Hashicorp Ansible
                <br>- Hashicorp Vault
                <br>- JFrog Artifactory
                <br>- SEQ
                <br>- Docker
                <br>- Kubernetes
                <br>- ElasticSearch / Beanstalk
                <br>- Kibana
                <br>- Jenkins
                <br>- Atlassian Suite (Jira, Bitbucket, Confluence)
                <br>- Powershell scripts
                <br>- Vagrant
                <br>- Grafana
                <br>- Integration of APM tools such as Stackify, NewRelic & DataDog`
              },
              {
                'id': '2',
                'from_to_month_year': 'April 2017 - November 2018',
                'organization': 'ADP',
                'logo': 'adp.png',
                'height': '"50px"',
                'designation': 'DevOps Engineer',
                'details': `- Integration and standarization of a continuous delivery / integration lifecycle
                <br>- Implementation of automated deployment systems using Jenkins, Artifactory, TFS, Git
                <br>- Monitorization of systems with Nagios
                <br>- Globalization and centralization of logging using Splunk
                <br>- Creation of Ansible playbooks for automated deployment of systems and services
                <br>- Segregation of API's and services with different endpoints
                
                <br><strong>Technologies</strong>
                <br>- Ansible
                <br>- Jenkins
                <br>- Git
                <br>- Splunk
                <br>- Nagios
                <br>- Bladelogic
                <br>- RESTful API with NodeJS services (using Express)`
              },
              {
                'id': '3',
                'from_to_month_year': 'April 2017 - November 2018',
                'organization': 'ADP',
                'logo': 'adp.png',
                'height': '"50px"',
                'designation': 'Full-Stack Front-End Developer',
                'details': `- Design and development of a Release Management Tool with MVC methodologies. 
                <br>- Design and development of a REST API service
                <br>- Design and development of a service that runs automated tests with Protractor / .NET with Selenium.
                <br>- Design and development of integration with third-party services like Artifactory, BitBucket, Jenkins, Archer, Rally (CA Technologies)
                <br>- Integration with external modules to a NodeJS platform, such as: jsPDF, ActiveDirectory, Jenkins, SQLite, Rally.
                <br><strong>Technologies</strong><br>
                - Development: HTML5, CSS3, jQuery, HTTP/REST, NodeJS, SailsJS. 
                <br>- Databases: MySQL, Oracle, MongoDB, SQLServer, SQLite.
                <br>- Scripting: BASH/Shell, Powershell, NodeJS. 
                <br>- Infrastructure / Platform: Windows, Linux, Docker, AWS, VMWare, VirtualBox. 
                <br>- Platform: Apache Web Server. 
                <br>- ALM: Artifactory, SonarQube, Jenkins, JIRA, TFS, SVN, Git, Selenium, Rally.`
              },
              {
                'id': '4',
                'from_to_month_year': 'April 2014 - April 2017',
                'organization': 'UserZoom',
                'logo': 'uz.png',
                'height': '"80px"',
                'designation': 'IT Security Engineer and Technical Customer Support',
                'details': `Address the objectives through technical leadership, advocacy and collaboration with Product Management, Engineering Technology Groups and customer facing organizations. The Security Technical Engineer will provide technical knowledge, advocacy and prioritization across a broad set of key security areas including the following: 
                <br>- File System Encryption, Retention, Sanitization 
                <br>- Trusted Computing 
                <br>- System Security 
                <br>- Authentication 
                <br>- Cross-Functional Vulnerability Analysis`
              },
              {
                'id': '5',
                'from_to_month_year': 'February 2013 - April 2017',
                'organization': 'SEUR',
                'logo': 'seur.png',
                'height': '"80px"',
                'designation': 'IT, Quality, Image and Security Manager',
                'details': `System Administrator
                <br>- AS400(IBM) Systems
                <br>- Network setup
                <br>- Switches configuration
                <br>- Proxy administration
                <br>- Cisco and D-Link router administration
                <br>- PHP, XML, WSDL, SOAP, Java and C# programming
                <br>- Web Services
                <br>- Client integration
                <br>- Helpdesk support
                <br>- Head of new systems training
                <br>- E-Commerce systems integration
                <br>- E-Commerce modules installation
                <br>- DVR System setup
                <br>- Web Service programming for Joyería TOUS
                <br>- Database and system maintenance`
              },
              {
                'id': '6',
                'from_to_month_year': 'September 2011 - June 2012',
                'organization': 'Deporvillage',
                'logo': 'deporvillage.png',
                'height': '"80px"',
                'designation': 'Web Developer',
                'details': `PHP Web developer for Magento
                <br>- Module implementation and configuration.
                <br>- Newsletter designing
                <br>- Banner designing
                <br>- Google Analytics integration`
              }
        ];
        return res.json(experience);
    },

    education: (req, res) => {
        console.log('Loading Education data');
        var education = [
            {
                'id': '1',
                'from_to_year': '2008 - 2012',
                'education': 'Bachelor\'s Degree',
                'stream': 'Computer Engineering',
                'institution': 'Barcelona, Spain'
            }
        ];
        return res.json(education);
    }
}