
# Portfolio Resume

### [Demo](https://afernandez.dev)


## Requirements
  - [Nodejs](https://nodejs.org) - 8.x or greater version
  - [NPM](https://www.npmjs.com/get-npm) - 5.x or greater version
  - [Angular-CLI](https://cli.angular.io)

## Installation
  - Run npm install in the root directory
  - For the contact form to work, you need an email transporter. By default it's configured to use GMAIL. Check the server/routes/email.js file, put your username and password/1time password.
  - Run the app with ng serve --aot

## Release
- ***Release 1.0.0*** - Initial Commit
