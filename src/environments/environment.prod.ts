import { SnotifyPosition } from 'ng-snotify';

export const environment = {
  production: true,
  baseUrl : `https://afernandez.dev/`,
  serverUrl: 'https://afernandez.dev:8080/',

  snotifyConfig : {
    showProgressBar: true,
    position: SnotifyPosition.rightTop,
  },
};
