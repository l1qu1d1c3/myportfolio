import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../profile.service';
import { SnotifyService } from 'ng-snotify';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  snotifyConfig = environment.snotifyConfig;
  model: any = {};

  constructor(
    private profile: ProfileService,
    private snotify: SnotifyService
  ) { }

  ngOnInit() {
  }

  contact() {
    console.log('Started contact method');
    this.profile.contactus(this.model).subscribe(data => {
      if (data.status) {
        console.log('Success');
        this.snotify.success(data.message, 'Success', this.snotifyConfig);
      } else {
        console.log('Warning');
        this.snotify.warning(data.message, 'Warning', this.snotifyConfig);
      }
    }, err => {
      this.snotify.error('Something went wrong. Try again later.', 'Error', this.snotifyConfig);
    });
  }

}
