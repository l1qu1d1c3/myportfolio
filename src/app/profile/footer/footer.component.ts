import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  profileData: any;
  constructor(private profile: ProfileService) { }

  ngOnInit() {
    this.getProfile();
  }

  getProfile(){
    this.profile.about().subscribe(data => {
      this.profileData = data;
    });;
  }

}
