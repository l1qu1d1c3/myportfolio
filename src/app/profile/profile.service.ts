import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  baseUrl = environment.baseUrl;
  serverUrl = environment.serverUrl

  constructor(
    private http: HttpClient
  ) { }

  educationData: any = [
    
  ];
  contactus(data: any): Observable<any> {
    return this.http.post(this.baseUrl + 'sendmail', data);
  }

  skills(): Observable<any> {
    return this.http.get(this.serverUrl + 'user/me/skills');
  }

  education(): Observable<any> {
    return this.http.get(this.serverUrl + 'user/me/education');
  }

  exprience(): Observable<any> {
    return this.http.get(this.serverUrl + 'user/me/experience');
  }

  public about(): Observable<any>{
    return this.http.get(this.serverUrl + 'user/me');
  }
}
