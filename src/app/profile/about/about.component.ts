import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../profile.service'

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  profileData: any;
  constructor(private profile: ProfileService) {

   }

  ngOnInit() {
    this.getProfile();
  }

  getProfile(){
    this.profileData = [];
    this.profile.about().subscribe(data => {
      this.profileData = data;
    });;
  }

}
