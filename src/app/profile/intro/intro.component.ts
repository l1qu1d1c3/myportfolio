import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../profile.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {
  profileData: any;

  constructor(private profile: ProfileService) { }

  ngOnInit() {
    this.getProfile();
  }
  
  getProfile(){
    this.profile.about().subscribe(data => {
      this.profileData = data;
    });
  }
  

}
